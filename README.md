# Residual Neural Network Architectures to Improve Prediction Accuracy of Properties of Materials

Properties in material composition and crystal struc- tures have been explored by density functional theory (DFT) cal- culations, using databases such as the Open Quantum Materials Database (OQMD). Databases like these have been used currently for the training of advanced machine learning and deep neural network models, the latter providing higher performance when predicting properties of materials. However, current alternatives have shown a deterioration in accuracy when increasing the number of layers in their architecture (over-fitting problem). As an alternative method to address this problem, we have implemented residual neural network architectures based on Merge and Run Networks, IRNet and UNet to improve per- formance while relaxing the observed network depth limitation. The evaluation of the proposed architectures include a 9:1 ratio to train and test as well as 10 fold cross validation. In the experiments we found that our proposed architectures based on IRNet and UNet are able to obtain a lower Mean Absolute Error (MAE) than current strategies. The full implementation (Python, Tensorflow and Keras) and the trained networks will be available online for community validation and advancing the state of the art from our findings.

### Environment installation
The file conda-env-spec-file.txt contains all the dependencies installed in the virtual environment where the training/testing was carried out. 

This file may be used to create an environment using:
```sh
$ conda create --name <env> --file conda-env-spec-file.tx
# platform: linux-64
```

# 


### Training

For training you must first download train_X.csv and train_y.csv. Tthe links for downloading are found within the respective files in the data folder.
Once you have the environment installed and the training data, you can start training the model with the following command:

```sh
$ python <name of the architecture>.py
```

### Testing the models
In the same way as when training, you must download the test data (test_X.csv and test_y.csv).
You must make sure that the weights of the trained model you want to test are on the same path. This path is inside the check-<model>.py script, in the line model.load_weights ("<checkpoint path> .h5")

Once you have installed the environment, downloaded the test data and located in the correct folder the model weights (.h5 format) you can test the architectures with the following command
```sh
$ python check-<name of the architecture>.py
```

