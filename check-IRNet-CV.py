import numpy as np
import os
from numpy import genfromtxt
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Lambda, BatchNormalization
from tensorflow.keras.layers import Convolution2D, MaxPooling2D, AveragePooling2D, Input,concatenate
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.optimizers import SGD
import matplotlib.pyplot as plt
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import KFold
from tensorflow.keras.utils import plot_model
import pandas as pd



def create_new_model():
    input_vec = Input(shape=(86,))
    x0=Activation('relu')(BatchNormalization()(Dense(1024)(input_vec)))
    m1=concatenate([input_vec, x0], axis=-1)
    x1=Activation('relu')(BatchNormalization()(Dense(1024)(m1)))
    m2=concatenate([m1, x1], axis=-1)
    x2=Activation('relu')(BatchNormalization()(Dense(1024)(m2)))
    m3=concatenate([m2, x2], axis=-1)
    x3=Activation('relu')(BatchNormalization()(Dense(1024)(m3)))
    
    m4=concatenate([m3, x3], axis=-1)
    x4=Activation('relu')(BatchNormalization()(Dense(512)(m4)))
    m5=concatenate([m4, x4], axis=-1)
    x5=Activation('relu')(BatchNormalization()(Dense(512)(m5)))
    m6=concatenate([m5, x5], axis=-1)
    x6=Activation('relu')(BatchNormalization()(Dense(512)(m6)))
    
    m7=concatenate([m6, x6], axis=-1)
    x7=Activation('relu')(BatchNormalization()(Dense(256)(m7)))
    m8=concatenate([m7, x7], axis=-1)
    x8=Activation('relu')(BatchNormalization()(Dense(256)(m8)))
    m9=concatenate([m8, x8], axis=-1)
    x9=Activation('relu')(BatchNormalization()(Dense(256)(m9)))
    
    m10=concatenate([m9, x9], axis=-1)
    x10=Activation('relu')(BatchNormalization()(Dense(128)(m10)))
    m11=concatenate([m10, x10], axis=-1)
    x11=Activation('relu')(BatchNormalization()(Dense(128)(m11)))
    m12=concatenate([m11, x11], axis=-1)
    x12=Activation('relu')(BatchNormalization()(Dense(128)(m12)))
    
    m13=concatenate([m12, x12], axis=-1)
    x13=Activation('relu')(BatchNormalization()(Dense(64)(m13)))
    m14=concatenate([m13, x13], axis=-1)
    x14=Activation('relu')(BatchNormalization()(Dense(64)(m14)))
    
    m15=concatenate([m14, x14], axis=-1)
    x15=Activation('relu')(BatchNormalization()(Dense(32)(m15)))
    
    m16=concatenate([m15, x15], axis=-1)
    x16=Dense(1,activation='linear')(m16)
    
    model=Model(input_vec,x16)
    # model.summary()
    return model


test_X = genfromtxt("data/test_X.csv", delimiter=',')
test_y = genfromtxt("data/test_y.csv", delimiter=',')

for i in range(0,9):
    model=create_new_model()
    model.load_weights("data/IRNet-CV/checkpoint-k-"+str(i)+".h5")
    prediction=model.predict(test_X)
    mae=mean_absolute_error(test_y,prediction)
    print("mae fold ",str(i)," : ",mae)
