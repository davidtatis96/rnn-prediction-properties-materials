import numpy as np
import os
from numpy import genfromtxt
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Lambda, BatchNormalization
from tensorflow.keras.layers import Convolution2D, MaxPooling2D, AveragePooling2D, Input,concatenate
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import ModelCheckpoint,EarlyStopping
from tensorflow.keras.optimizers import SGD
import matplotlib.pyplot as plt
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import KFold
from tensorflow.keras.utils import plot_model
import pandas as pd




train_X = genfromtxt("data/train_X.csv", delimiter=',')
train_y = genfromtxt("data/train_Y.csv", delimiter=',')
test_X = genfromtxt("data/test_X.csv", delimiter=',')
test_y = genfromtxt("data/test_y.csv", delimiter=',')
print(train_X.shape)

def create_new_model():
    input_vec = Input(shape=(86,))
    #encoder
    x0=Activation('relu')(BatchNormalization()(Dense(1024)(input_vec)))
    x1=Activation('relu')(BatchNormalization()(Dense(1024)(x0)))
    x2=Activation('relu')(BatchNormalization()(Dense(1024)(x1)))
    x3=Activation('relu')(BatchNormalization()(Dense(1024)(x2)))

    x4=Activation('relu')(BatchNormalization()(Dense(512)(x3)))
    x5=Activation('relu')(BatchNormalization()(Dense(512)(x4)))
    x6=Activation('relu')(BatchNormalization()(Dense(512)(x5)))

    x7=Activation('relu')(BatchNormalization()(Dense(256)(x6)))
    x8=Activation('relu')(BatchNormalization()(Dense(256)(x7)))
    x9=Activation('relu')(BatchNormalization()(Dense(256)(x8)))

    x10=Activation('relu')(BatchNormalization()(Dense(128)(x9)))
    x11=Activation('relu')(BatchNormalization()(Dense(128)(x10)))
    x12=Activation('relu')(BatchNormalization()(Dense(128)(x11)))

    x13=Activation('relu')(BatchNormalization()(Dense(64)(x12)))
    x14=Activation('relu')(BatchNormalization()(Dense(64)(x13)))
    x15=Activation('relu')(BatchNormalization()(Dense(64)(x14)))

    #decoder
    merge1=concatenate([x12, x15], axis=-1)
    x16=Activation('relu')(BatchNormalization()(Dense(128)(merge1)))
    x17=Activation('relu')(BatchNormalization()(Dense(128)(x16)))
    x18=Activation('relu')(BatchNormalization()(Dense(128)(x17)))

    merge2=concatenate([x9, x18], axis=-1)
    x19=Activation('relu')(BatchNormalization()(Dense(256)(merge2)))
    x20=Activation('relu')(BatchNormalization()(Dense(256)(x19)))
    x21=Activation('relu')(BatchNormalization()(Dense(256)(x20)))

    merge3=concatenate([x6, x21], axis=-1)
    x22=Activation('relu')(BatchNormalization()(Dense(256)(merge3)))
    x23=Activation('relu')(BatchNormalization()(Dense(256)(x22)))
    x24=Activation('relu')(BatchNormalization()(Dense(128)(x23)))

    merge4=concatenate([x3, x24], axis=-1)
    x25=Activation('relu')(BatchNormalization()(Dense(64)(merge4)))
    x26=Activation('relu')(BatchNormalization()(Dense(32)(x25)))
    x27=Activation('relu')(BatchNormalization()(Dense(16)(x26)))

    x28=Dense(1, activation='linear')(x27) #53

    model=Model(input_vec,x28)
    # model.summary()
    return model


k_folds=10
kf = KFold(n_splits=k_folds, shuffle=True, random_state=1234)
train_Xi=[[] for each in range(k_folds)]
train_Yi=[[] for each in range(k_folds)]
valid_Xi=[[] for each in range(k_folds)]
valid_Yi=[[] for each in range(k_folds)]
i=0

for train_index, val_index in kf.split(train_X):
    train_Xi[i] = pd.DataFrame(train_X).iloc[train_index,]
    train_Yi[i] = pd.DataFrame(train_y).iloc[train_index,]
    
    valid_Xi[i]= pd.DataFrame(train_X).iloc[val_index,]
    valid_Yi[i]= pd.DataFrame(train_y).iloc[val_index,]
    
    i=i+1



mae_test=[-1 for each in range(k_folds)]
for i in range(1,k_folds):
  model=create_new_model()
  callback_checkpoint = ModelCheckpoint(
      "data/UNET_BN_RELU/checkpoint-k-"+str(i)+".h5",
      verbose=1,
      monitor='val_loss',
      save_best_only=True
  )
  model.compile(
      optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
      loss=tf.keras.losses.mean_absolute_error,
      metrics=[tf.keras.metrics.MeanAbsoluteError()])
  
  history = model.fit(
      train_Xi[i],
      train_Yi[i],
      batch_size=64,
      epochs=10000,
      callbacks=[callback_checkpoint,EarlyStopping(patience=200)],
      validation_data=(valid_Xi[i],valid_Yi[i]),
  )
  f=open("data/UNET_BN_RELU/"+str(i)+"-epochs-"+str(len(history.history['loss']))+".txt","w+")

