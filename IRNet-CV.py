import numpy as np
import os
from numpy import genfromtxt
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Lambda, BatchNormalization
from tensorflow.keras.layers import Convolution2D, MaxPooling2D, AveragePooling2D, Input,concatenate
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import ModelCheckpoint,EarlyStopping
from tensorflow.keras.optimizers import SGD
import matplotlib.pyplot as plt
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import KFold
from tensorflow.keras.utils import plot_model
import pandas as pd




train_X = genfromtxt("data/train_X.csv", delimiter=',')
train_y = genfromtxt("data/train_Y.csv", delimiter=',')
test_X = genfromtxt("data/test_X.csv", delimiter=',')
test_y = genfromtxt("data/test_y.csv", delimiter=',')
print(train_X.shape)

def create_new_model():
    input_vec = Input(shape=(86,))
    x0=Activation('relu')(BatchNormalization()(Dense(1024)(input_vec)))
    m1=concatenate([input_vec, x0], axis=-1)
    x1=Activation('relu')(BatchNormalization()(Dense(1024)(m1)))
    m2=concatenate([m1, x1], axis=-1)
    x2=Activation('relu')(BatchNormalization()(Dense(1024)(m2)))
    m3=concatenate([m2, x2], axis=-1)
    x3=Activation('relu')(BatchNormalization()(Dense(1024)(m3)))

    m4=concatenate([m3, x3], axis=-1)
    x4=Activation('relu')(BatchNormalization()(Dense(512)(m4)))
    m5=concatenate([m4, x4], axis=-1)
    x5=Activation('relu')(BatchNormalization()(Dense(512)(m5)))
    m6=concatenate([m5, x5], axis=-1)
    x6=Activation('relu')(BatchNormalization()(Dense(512)(m6)))
    
    m7=concatenate([m6, x6], axis=-1)
    x7=Activation('relu')(BatchNormalization()(Dense(256)(m7)))
    m8=concatenate([m7, x7], axis=-1)
    x8=Activation('relu')(BatchNormalization()(Dense(256)(m8)))
    m9=concatenate([m8, x8], axis=-1)
    x9=Activation('relu')(BatchNormalization()(Dense(256)(m9)))
    
    m10=concatenate([m9, x9], axis=-1)
    x10=Activation('relu')(BatchNormalization()(Dense(128)(m10)))
    m11=concatenate([m10, x10], axis=-1)
    x11=Activation('relu')(BatchNormalization()(Dense(128)(m11)))
    m12=concatenate([m11, x11], axis=-1)
    x12=Activation('relu')(BatchNormalization()(Dense(128)(m12)))
    
    m13=concatenate([m12, x12], axis=-1)
    x13=Activation('relu')(BatchNormalization()(Dense(64)(m13)))
    m14=concatenate([m13, x13], axis=-1)
    x14=Activation('relu')(BatchNormalization()(Dense(64)(m14)))
    
    m15=concatenate([m14, x14], axis=-1)
    x15=Activation('relu')(BatchNormalization()(Dense(32)(m15)))
    
    m16=concatenate([m15, x15], axis=-1)
    x16=Dense(1,activation='linear')(m16)
    
    model=Model(input_vec,x16)
    # model.summary()
    return model

k_folds=10
kf = KFold(n_splits=k_folds, shuffle=True, random_state=1234)
train_Xi=[[] for each in range(k_folds)]
train_Yi=[[] for each in range(k_folds)]
valid_Xi=[[] for each in range(k_folds)]
valid_Yi=[[] for each in range(k_folds)]
i=0

for train_index, val_index in kf.split(train_X):
    train_Xi[i] = pd.DataFrame(train_X).iloc[train_index,]
    train_Yi[i] = pd.DataFrame(train_y).iloc[train_index,]
    
    valid_Xi[i]= pd.DataFrame(train_X).iloc[val_index,]
    valid_Yi[i]= pd.DataFrame(train_y).iloc[val_index,]
    
    i=i+1



#os.environ["CUDA_VISIBLE_DEVICES"]="0"

mae_test=[-1 for each in range(k_folds)]
for i in range(0,k_folds):
  model=create_new_model()
  callback_checkpoint = ModelCheckpoint(
      "data/IRNET/checkpoint-k-"+str(i)+".h5",
      verbose=1,
      monitor='val_loss',
      save_best_only=True
  )
  model.compile(
      optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
      loss=tf.keras.losses.mean_absolute_error,
      metrics=[tf.keras.metrics.MeanAbsoluteError()])
  
  plot_model(model)
  history = model.fit(
      train_Xi[i],
      train_Yi[i],
      batch_size=64,
      epochs=10000,
      callbacks=[callback_checkpoint,EarlyStopping(patience=200)],
      validation_data=(valid_Xi[i],valid_Yi[i]),
  )
  f=open("data/IRNET/"+str(i)+"-epochs-"+str(len(history.history['loss']))+".txt","w+")
